// Conditional Statements

// [SECTION] if, else is, else Statement

let numA = 5;
let numB = 1;

if(numA > 3) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}

if(numA < 3) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}

// let city = "New York";

// if (city === "New York") {
	
// 	console.log("Welcome to " + city + " City");
// }

city = "Tokyo";
 
if (city === "New York") {
	console.log("Welcome to New York City!")
} else if(city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan!")
}

let numC = -5;
let numD = 7;

if (numC > 0) {
	console.log("Hello");
} else if (numD === 0) {
	console.log("World");
} else {
	console.log("Again");
}

// ============

function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if (windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if (windSpeed >= 62 && windSpeed <= 88) { // && means AND operator
		return 'Tropical storm detected.';
	}
	else if (windSpeed >= 89 && windSpeed <= 117) { // 
		return 'Severe Tropical Storm detected.';
	}
	else {
		return 'Typhoon detected.';
	}
}

let message = determineTyphoonIntensity(120);
console.log(message);

if (message == "Tropical storm detected.") {
	console.warn(message);
}

// [SECTION] Conditional (Ternary) Operator

// Single statement execution
	// - commonly used for single statement execution where the result consists of only one line of code.

// - Syntax
	// (expression) ? ifTrue : ifFalse;

let t = "yes";
let f = "no";


let ternaryResult = (1 < 18) ? t : f
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement execution

let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// The "parseInt" function converts the input receive into a number data type.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// [SECTION] Switch Statement
	// - the "switch" statements evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	// - can be used an alternative to an "if, else if and else" statement where the data to be used in the condition in an expected output.

// Expression


	// switch (expression) {
	// case value;
	// 	statement;
	// 	break;
	// default: statement;
	// }

let day = prompt("What day is it today?").toLowerCase();
console.log(day);


switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}







